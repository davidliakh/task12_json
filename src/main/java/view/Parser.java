package view;

import java.io.File;
import controller.parser.JSONParser;

public class Parser {

  public static void main(String[] args) {
   File json = new File("src\\main\\resources\\knifes.json");
   File schema = new File("src\\main\\resources\\knifesScheme.json");
    JSONParser jsonParser = new JSONParser(json,schema);
    jsonParser.getKnifeList().stream().map(k -> k.getInfo()).forEach(System.out::println);
  }
}
