package controller.parser;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import model.Knife;

public class JSONParser {
  private ObjectMapper objectMapper;
  private List<Knife>knifeList;
  public JSONParser(File jsonFile, File jsonSchema){
    objectMapper = new ObjectMapper();
    try {
      if(JSONValidator.validateSchema(jsonFile,jsonSchema)) {
        knifeList = Arrays.asList(objectMapper.readValue(jsonFile, Knife[].class));
      }
      else {
        System.out.println("File is not valid!");
      }
    }
    catch (JsonParseException| JsonMappingException| ProcessingException e){
      e.getStackTrace();
    }
    catch (IOException ex){
      ex.getStackTrace();
    }
  }

  public List<Knife> getKnifeList() {
    return knifeList;
  }
}
