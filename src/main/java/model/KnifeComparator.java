package model;

import java.util.Comparator;
import model.Knife;

public class KnifeComparator implements Comparator<Knife> {

  @Override
  public int compare(Knife o1, Knife o2) {
    return o2.getId() - o1.getId();
  }
}
