package model;

public class Knife {

  private int id;
  private String type;
  private String handy;
  private String origin;
  private Visual visual;
  private boolean collectible;

  public Knife(int id, String type, String handy, String origin,
      Visual visual,
      boolean collectible) {
    this.id = id;
    this.type = type;
    this.handy = handy;
    this.origin = origin;
    this.visual = visual;
    this.collectible = collectible;
  }

  @Override
  public String toString() {
    return "model.Knife{" +
        "id=" + id +
        ", type='" + type + '\'' +
        ", handy='" + handy + '\'' +
        ", origin='" + origin + '\'' +
        ", visualChar=" + visual +
        ", collectible=" + collectible +
        '}';
  }

  public String getInfo(){
    return "model.Knife{" +
        "id=" + id +
        ", type='" + type + '\'' +
        ", handy='" + handy + '\'' +
        ", origin='" + origin + '\'' +
        ", blade length=" + visual.getBlade().getLength() +
        ", blade width=" + visual.getBlade().getWidth() +
        ", blade material=" + visual.getBlade().getMaterial() +
        ", handle material=" + visual.getHandle().getMaterial() +
        ", handle details=" + visual.getHandle().getDetails() +
        ", bloodstream=" + visual.getBlade().isBloodstream() +
        ", collectible=" + collectible +
        '}';
  }

  public Knife() {
  }

  public Visual getVisual() {
    return visual;
  }

  public void setVisual(Visual visual) {
    this.visual = visual;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getHandy() {
    return handy;
  }

  public void setHandy(String handy) {
    this.handy = handy;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public boolean collectible() {
    return collectible;
  }

  public void setCollectible(boolean collectible) {
    this.collectible = collectible;
  }
}
