package model;

public class Handle {

  private String material;
  private String details;

  Handle(String material, String detail) {
    this.details = details;
    this.material = material;
  }

  public Handle() {
  }

  public String getMaterial() {
    return material;
  }

  public void setMaterial(String material) {
    this.material = material;
  }

  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }
}
